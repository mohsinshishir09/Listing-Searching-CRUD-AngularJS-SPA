'use strict';

/**
 * @ngdoc function
 * @name topMoviesApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the topMoviesApp
 */
angular.module('topMoviesApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
