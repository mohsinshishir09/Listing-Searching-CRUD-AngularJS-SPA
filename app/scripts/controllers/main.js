'use strict';

/**
 * @ngdoc function
 * @name topMoviesApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the topMoviesApp
 */
angular.module('topMoviesApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
