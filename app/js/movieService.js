
'use strict';

var movieService = movieApp.factory('movieService', function() {

 	// var savedData = {};
 	var savedData = [];

 	function set(data) {
	   	// savedData.id = data.id;
	   	// savedData.rank = data.rank;
	   	// savedData.title = data.title;
	   	savedData.push(data);
	}

	function get() {
	  	return savedData;
	}

	return {
	  	set: set,
	  	get: get
	}

});

// var movieService = movieApp.factory("movieService", ['$http', function($http) {
//         return {
//         	getResponders:  function() {
//         	$http({
//       method: 'GET',
//       // url: 'http://api.kidorkar.com/api/admin/orders'
//       url: 'https://raw.githubusercontent.com/hjorturlarsen/IMDB-top-100/master/data/movies.json'
//     }).then(function (success){
//     	console.log(success.data);
//     	return success.data;
//     },function (error){
// 		console.log('error');
// 		console.log(error);
//     });
//         }
//         };
// }]);


// return {
//         getResponders: function() {    
//             return $http.get('https://raw.githubusercontent.com/hjorturlarsen/IMDB-top-100/master/data/movies.json')
//             .then(function(response) {
//                 console.log("coming from servicejs", response.data);
//                 //return data when promise resolved
//                 //that would help you to continue promise chain.
//                 return response.data;
//             });
//         }
//     };