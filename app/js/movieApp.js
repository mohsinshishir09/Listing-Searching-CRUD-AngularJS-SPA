
'use strict';

var movieApp = angular.module('movieApp', ['ngRoute']);

movieApp.config(function($routeProvider, $locationProvider) {

    $routeProvider
    .when("/", {

        templateUrl : "views/content-home.html",
        controller : "movieController"

    })
    .when("/insert", {

        templateUrl : "views/content-insert.html",
        controller : "movieController"

    })
    .when("/edit/:id", {

        templateUrl : "views/content-edit.html",
        controller : "movieController"

    })
    .when("/detail", {

        templateUrl : "views/content-detail.html",
        controller : "movieController"

    })
    .when("/demo", {

        templateUrl : "views/demo.html",
        controller : "movieController"

    })
    .otherwise({
        redirectTo: '/'
    });

    /*This code is used for clean url*/
    $locationProvider.html5Mode({
	   	enabled: true,
	   	requireBase: false
	});

});

/*This code is used for mutiple routing in a single Controller*/
movieApp.provider('appState', function() {

    this.$get = [function() {
        return {
            data: {}
        };
    }];

});